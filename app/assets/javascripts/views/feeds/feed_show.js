NewsReader.Views.FeedShow = Backbone.View.extend({

  initialize: function() {
    this.listenTo(this.model, "sync", this.render);
    // must render the view again once the collection is fetched
  },

  template: JST['feeds/show'],

  render: function() {
    var renderedContent = this.template({
      feed: this.model
    });

    this.$el.html(renderedContent);  ///$el = ????, empty, all content put inside a div on page

    return this;
  }


});